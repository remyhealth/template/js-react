import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';
import properties from './properties';



export default combineReducers({
    properties,
    router: routerReducer,

});