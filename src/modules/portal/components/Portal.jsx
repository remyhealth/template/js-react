import * as React from 'react';
import {Route} from 'react-router-dom';
import Example from './Example';

export class Portal extends React.Component {
    render() {
        return(
            <div>
                <Route exact path='/' component={Example}/>
            </div>
        )
    }
}